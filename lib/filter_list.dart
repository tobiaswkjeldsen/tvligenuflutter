import 'dart:async';
import 'package:flutter/material.dart';
import 'package:tvligenuflutter/date_helper.dart';
import 'package:tvligenuflutter/filter_item.dart';
import 'package:tvligenuflutter/program_description.dart';
import 'package:tvligenuflutter/program_item.dart';
import 'package:tvligenuflutter/search_item.dart';
import 'package:tvligenuflutter/settings_service.dart';
import 'package:tvligenuflutter/tv_assembler.dart';
import 'package:tvligenuflutter/tv_service.dart';

class _FilterListState extends State<FilterList> {
  var _tvService = new TVService();
  var _settingsService = new SettingsService();

  DateTime _startDate = new DateTime(new DateTime.now().year,
      new DateTime.now().month, new DateTime.now().day);
  DateTime _currentDate = new DateTime(new DateTime.now().year,
      new DateTime.now().month, new DateTime.now().day);

  String _title;
  String _category;
  String _channelName;

  List<Program> _programs = new List<Program>();

  _FilterListState(String title, String category, String channelName) {
    this._title = title;
    this._category = category;
    this._channelName = channelName;

    _filterPrograms();
  }

  void _pickDate() {
    showDatePicker(
        context: context,
        initialDate: _currentDate,
        firstDate: _startDate,
        lastDate: new DateTime.now().add(new Duration(days: 5))).then((date) {
      _currentDate = new DateTime(date.year, date.month, date.day);

      _filterPrograms();
    });
  }

  Future _filterPrograms() async {
    List<Channel> channels = await _settingsService.getChannels();
    List<Program> programs = await _tvService.searchPrograms(
        category: _category,
        day: _currentDate.difference(_startDate).inDays,
        channelName: _channelName);
    List<Program> _programsFiltered = new List<Program>();

    // Filter programs according to selected channels from settings
    channels.forEach((channel) {
      programs.forEach((program) {
        if (channel.name == program.channelName) {
          _programsFiltered.add(program);
        }
      });
    });

    if (channels.length == 0) {
      _programsFiltered.addAll(programs);
    }

    setState(() {
      _programs = _programsFiltered;
    });
  }

  void _previousDay() {
    _currentDate = _currentDate.subtract(new Duration(days: 1));

    _filterPrograms();
  }

  void _nextDay() {
    _currentDate = _currentDate.add(new Duration(days: 1));

    _filterPrograms();
  }

  Row _getTitle() {
    return new Row(children: [
      new Text(_title),
      new Padding(padding: const EdgeInsets.all(5.0)),
      new Text(DateHelper.todayTomorrowOrWeekDay(_currentDate),
          style: new TextStyle(fontSize: 14.0))
    ]);
  }

  void _onProgramPressed(BuildContext context, Program program) {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        return new ProgramDescription(program: program);
      },
    ));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: _getTitle(),
          actions: [
            new IconButton(
                icon: new Icon(Icons.calendar_today, color: Colors.white),
                onPressed: _pickDate),
            new IconButton(
                icon: new Icon(Icons.chevron_left, color: Colors.white),
                onPressed: _previousDay),
            new IconButton(
                icon: new Icon(Icons.chevron_right, color: Colors.white),
                onPressed: _nextDay),
          ],
        ),
        body: new ListView(
          children: _channelName != ""
              ? _programs.map((Program program) {
                  return new ProgramItem(
                    program: program,
                    onPressed: (Program program) {
                      _onProgramPressed(context, program);
                    },
                  );
                }).toList()
              : _programs.map((Program program) {
                  return new FilterItem(
                    program: program,
                    onPressed: (Program program) {
                      _onProgramPressed(context, program);
                    },
                  );
                }).toList(),
        ));
  }

  @override
  void dispose() {
    super.dispose();

    _programs = null;
  }
}

class FilterList extends StatefulWidget {
  FilterList({Key key, this.title, this.category, this.channelName})
      : super(key: key);

  final String title, category, channelName;

  @override
  _FilterListState createState() =>
      new _FilterListState(title, category, channelName);
}
