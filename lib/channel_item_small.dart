import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:tvligenuflutter/time.dart';
import 'package:tvligenuflutter/date_helper.dart';
import 'package:tvligenuflutter/tv_assembler.dart';

typedef void ChannelItemActionCallback(Overview overview);

class ChannelItemSmall extends StatelessWidget {
  ChannelItemSmall({Key key, this.overview, this.onPressed}) : super(key: key);

  final Overview overview;
  final ChannelItemActionCallback onPressed;

  @override
  Widget build(BuildContext context) {
    var length = overview.airTimeNext.millisecondsSinceEpoch -
        overview.airTime.millisecondsSinceEpoch;
    var progress = overview.airTimeNext.millisecondsSinceEpoch -
        new DateTime.now().millisecondsSinceEpoch;
    double percent = (((length - progress) * 100) / length) / 100;
    return new Container(
        padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 2.0),
        child: new Material(
            borderRadius: new BorderRadius.all(new Radius.circular(4.0)),
            elevation: 2.0,
            type: MaterialType.card,
            child: new InkWell(
                onTap: () {
                  onPressed(overview);
                },
                child: new Container(
                    decoration: new BoxDecoration(),
                    padding: const EdgeInsets.fromLTRB(20.0, 16.0, 20.0, 16.0),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        new Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            new ConstrainedBox(
                              constraints: const BoxConstraints(
                                maxWidth: 140.0,
                              ),
                              child: new Text(
                                overview.channelName,
                                style: new TextStyle(
                                  color: Colors.black54,
                                  fontSize: 19.0,
                                  fontWeight: FontWeight.w400,
                                ),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            new Padding(padding: const EdgeInsets.all(5.0)),
                            new Time(
                              time: overview.airTime,
                            ),
                            new Padding(padding: const EdgeInsets.all(5.0)),
                            new Expanded(
                                child: new Text(overview.programNameNow,
                                    style: new TextStyle(
                                        fontSize: 19.0,
                                        fontWeight: FontWeight.w300))),
                          ],
                        ),
                        new Padding(padding: const EdgeInsets.all(2.5)),
                        new LinearProgressIndicator(value: percent),
                        new Padding(padding: const EdgeInsets.all(2.5)),
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            new Text(
                                DateHelper.hoursMinutes(overview.airTimeNext),
                                style: new TextStyle(
                                    color: Colors.grey,
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.w300)),
                            new Padding(padding: const EdgeInsets.all(5.0)),
                            new Text(overview.programNameNext,
                                style: new TextStyle(
                                    color: Colors.black54,
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.w300)),
                          ],
                        ),
                      ],
                    )))));
  }
}
