import 'dart:async';
import 'package:flutter/material.dart';
import 'package:tvligenuflutter/alarm_service.dart';
import 'package:tvligenuflutter/date_helper.dart';
import 'package:tvligenuflutter/program_description.dart';
import 'package:tvligenuflutter/tv_assembler.dart';

class _AlarmListState extends State<AlarmList> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  var _alarmService = new AlarmService();

  List<ProgramAlarm> _programAlarms = new List<ProgramAlarm>();


  @override
  void initState() {
    super.initState();

    _fetchAlarms();
  }

  Future _fetchAlarms() async {
    await _alarmService.init();

    setState(() {
      _programAlarms = _alarmService.getProgramAlarms();
    });
  }

  Future _confirmUnregisterAlarm(Program program) async {
    await _alarmService.unregister(program);

    _fetchAlarms();
  }

  Future _unregisterAlarm(Program program) async {
    showDialog(
        context: context,
        builder: (_) => new AlertDialog(
            title: const Text("Fjern alarm"),
            content: const Text("Vil du fjerne denne alarm?"),
            actions: <Widget>[
              new FlatButton(
                  child: const Text('Nej'),
                  onPressed: () {
                    Navigator.pop(context, false);
                  }),
              new FlatButton(
                  child: const Text('Ja'),
                  onPressed: () {
                    _confirmUnregisterAlarm(program);
                    Navigator.pop(context, true);
                  }),
            ]));
  }

  void _programAlarmTapped(BuildContext context, ProgramAlarm programAlarm) {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        return new ProgramDescription(program: programAlarm.program);
      },
    ));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(title: new Text("Alarmer")),
      body: new ListView(
        children: _programAlarms.map((ProgramAlarm programAlarm) {
          return new Container(
              padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 3.0),
              child: new Material(
                  borderRadius: new BorderRadius.all(new Radius.circular(4.0)),
                  elevation: 2.0,
                  type: MaterialType.card,
                  child: new InkWell(
                onTap: () {
                  _programAlarmTapped(context, programAlarm);
                },
                child: new Container(
                    padding: const EdgeInsets.fromLTRB(20.0, 16.0, 20.0, 16.0),
                    child: new Column(children: [
                      new Text(programAlarm.program.channelName,
                          style: new TextStyle(
                              color: Colors.black54,
                              fontSize: 19.0,
                              fontWeight: FontWeight.w400)),
                      new Padding(padding: const EdgeInsets.all(5.0)),
                      new Text(programAlarm.program.programName,
                          style: new TextStyle(
                              fontSize: 19.0,
                              fontWeight: FontWeight.w300)),
                      new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            new Text(
                                "Programmet starter ${DateHelper
                                      .todayTomorrowOrDateWithMinutes(programAlarm.program
                                      .airTime)}",
                                style: new TextStyle(
                                    color: Colors.grey,
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.w300)),
                            new IconButton(
                              icon: new Icon(Icons.alarm_off),
                              onPressed: () {
                                _unregisterAlarm(programAlarm.program);
                              },
                            )
                          ])
                    ], crossAxisAlignment: CrossAxisAlignment.start)),
              )));
        }).toList(),
      ),
    );
  }
}

class AlarmList extends StatefulWidget {
  AlarmList({Key key}) : super(key: key);

  @override
  _AlarmListState createState() => new _AlarmListState();
}
