import 'package:flutter/material.dart';

import 'package:tvligenuflutter/about.dart';
import 'package:tvligenuflutter/settings_service.dart';
import 'setting_types.dart';

var settingsService = new SettingsService();

class Settings extends StatefulWidget {
  const Settings(this.configuration, this.updater);

  final Setting configuration;
  final ValueChanged<Setting> updater;

  @override
  SettingsState createState() => new SettingsState();
}

class SettingsState extends State<Settings> {
  double _sliderValue = 0.0;

  SettingsState() {
    settingsService.getAlarmOffsetMinutes().then((offset) {
      setState(() {
        _sliderValue = offset.toDouble();
      });
    });
  }

  void _handleThemeChanged(ThemeColor color) {
    sendUpdates(widget.configuration.copyWith(theme: color));
    settingsService.saveTheme(color.index);
    Navigator.pop(context, true);
  }

  _onAboutPressed() {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        return new About();
      },
    ));
  }

  void _changeTheme() {
    showDialog<bool>(
      context: context,
      child: new SimpleDialog(title: const Text("Farveindstilling"), children: [
        new ListTile(
            title: new Text("Standard"),
            subtitle: new Container(
              decoration: new BoxDecoration(
                border: new Border.all(
                  color: Colors.blueGrey,
                  width: 20.0,
                ),
              ),
            ),
            onTap: () {
              _handleThemeChanged(ThemeColor.standard);
            }),
        new ListTile(
            title: new Text("Grå"),
            subtitle: new Container(
              decoration: new BoxDecoration(
                border: new Border.all(
                  color: Colors.grey,
                  width: 20.0,
                ),
              ),
            ),
            onTap: () {
              _handleThemeChanged(ThemeColor.grey);
            }),
        new ListTile(
            title: new Text("Grøn"),
            subtitle: new Container(
              decoration: new BoxDecoration(
                border: new Border.all(
                  color: Colors.green,
                  width: 20.0,
                ),
              ),
            ),
            onTap: () {
              _handleThemeChanged(ThemeColor.green);
            }),
        new ListTile(
            title: new Text("Rød"),
            subtitle: new Container(
              decoration: new BoxDecoration(
                border: new Border.all(
                  color: Colors.red,
                  width: 20.0,
                ),
              ),
            ),
            onTap: () {
              _handleThemeChanged(ThemeColor.red);
            }),
        new ListTile(
            title: new Text("Blå"),
            subtitle: new Container(
              decoration: new BoxDecoration(
                border: new Border.all(
                  color: Colors.blue,
                  width: 20.0,
                ),
              ),
            ),
            onTap: () {
              _handleThemeChanged(ThemeColor.blue);
            }),
        new ListTile(
            title: new Text("Lilla"),
            subtitle: new Container(
              decoration: new BoxDecoration(
                border: new Border.all(
                  color: Colors.purple,
                  width: 20.0,
                ),
              ),
            ),
            onTap: () {
              _handleThemeChanged(ThemeColor.purple);
            }),
      ]),
    );
  }

  void sendUpdates(Setting value) {
    if (widget.updater != null) widget.updater(value);
  }

  Widget buildSettingsPane(BuildContext context) {
    final List<Widget> rows = <Widget>[
      new ListTile(
        leading: new Icon(Icons.alarm),
        title: new Text('Alarmindstilling'),
        subtitle: new Column(
          children: <Widget>[
            new Slider(
                value: _sliderValue,
                min: 0.0,
                max: 30.0,
                divisions: 6,
                onChanged: (double value) {
                  setState(() {
                    _sliderValue = value;
                    settingsService.saveAlarmOffset(value.toInt());
                  });
                }),
            new Text("Udløs ${_sliderValue.toInt()
                .toString()} minutter før programstart")
          ],
        ),
      ),
      new ListTile(
        leading: new Icon(Icons.color_lens),
        title: const Text('Farveindstilling'),
        onTap: _changeTheme,
      ),
      new ListTile(
          leading: new Icon(Icons.info),
          title: new Text("Om"),
          onTap: _onAboutPressed),
    ];
    return new ListView(
      padding: const EdgeInsets.symmetric(vertical: 20.0),
      children: rows,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(title: const Text('Indstillinger')),
        body: buildSettingsPane(context));
  }
}
