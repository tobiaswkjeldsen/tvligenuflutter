import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:tvligenuflutter/time.dart';
import 'package:tvligenuflutter/date_helper.dart';
import 'package:tvligenuflutter/tv_assembler.dart';

typedef void ProgramItemActionCallback(Program program);

class ProgramItem extends StatelessWidget {
  ProgramItem(
      {Key key, this.overview, this.program, this.hasAlarm, this.onPressed})
      : super(key: key);

  final Overview overview;
  final Program program;
  final bool hasAlarm;
  final ProgramItemActionCallback onPressed;

  @override
  Widget build(BuildContext context) {
    if (overview != null) {
      var length = program.airTimeEnd.millisecondsSinceEpoch -
          program.airTime.millisecondsSinceEpoch;
      var progress = program.airTimeEnd.millisecondsSinceEpoch -
          new DateTime.now().millisecondsSinceEpoch;
      double percent = (((length - progress) * 100) / length) / 100;
      return new Container(
          child: new InkWell(
              onTap: () {
                onPressed(program);
              },
              child: new Container(
                child: new SizedBox(
                  height: program.graphicsUrl.isNotEmpty ? 150.0 : 150.0,
                  child: new Stack(
                    fit: StackFit.expand,
                    children: <Widget>[
                      new Positioned.fill(
                          child: new Opacity(
                        opacity: 1.0,
                        child: new FadeInImage.assetNetwork(
                          placeholder: 'images/placeholder.png',
                          image: program.graphicsUrl,
                          fit: BoxFit.cover,
                        ),
                      )),
                      const DecoratedBox(
                        decoration: const BoxDecoration(
                          gradient: const LinearGradient(
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter,
                            colors: const <Color>[
                              Colors.black87,
                              Colors.transparent
                            ],
                          ),
                        ),
                      ),
                      new Positioned(
                        bottom: 16.0,
                        left: 20.0,
                        right: 20.0,
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Text("Lige nu",
                                style: new TextStyle(
                                    color: Colors.white,
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.w300)),
                            new Padding(padding: const EdgeInsets.all(5.0)),
                            new Text(program.programName,
                                style: new TextStyle(
                                    color: Colors.white,
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.w400)),
                            new Padding(padding: const EdgeInsets.all(2.5)),
                            new LinearProgressIndicator(value: percent),
                            new Padding(padding: const EdgeInsets.all(5.0)),
                            new Text(
                                "Programmet startede " +
                                    DateHelper.hoursMinutes(program.airTime),
                                style: new TextStyle(
                                    color: Colors.white70,
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.w300)),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )));
    } else {
      return new Container(
          child: new InkWell(
              onTap: () {
                onPressed(program);
              },
              child: new Container(
                  padding: const EdgeInsets.fromLTRB(20.0, 16.0, 20.0, 16.0),
                  decoration: new BoxDecoration(
                      border: new Border(
                          bottom: new BorderSide(
                              color: Theme.of(context).dividerColor))),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      new Time(time: program.airTime),
                      new Padding(padding: const EdgeInsets.all(5.0)),
                      new Expanded(child:                       new Text(program.programName,
                          style: new TextStyle(
                              fontSize: 19.0,
                              fontWeight: FontWeight.w300))),
                    ],
                  ))));
    }
  }
}
