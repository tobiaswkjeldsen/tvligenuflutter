import 'package:flutter/material.dart';
import 'package:tvligenuflutter/channel_list.dart';
import 'package:tvligenuflutter/date_helper.dart';
import 'package:tvligenuflutter/settings_service.dart';
import 'package:tvligenuflutter/setting_types.dart';
import 'package:tvligenuflutter/settings.dart';

var settingsService = new SettingsService();

void main() {
  runApp(new MyApp());
}

class MyApp extends StatefulWidget {
  @override
  MyAppState createState() => new MyAppState();
}

class MyAppState extends State<MyApp> {

  MyAppState() {
    settingsService.getTheme().then((themeColor){
      configurationUpdater(_configuration.copyWith(theme: ThemeColor.values[themeColor]));
    });

    DateHelper.initLocale();
  }

  Setting _configuration = new Setting(
    theme: ThemeColor.standard,
  );

  void configurationUpdater(Setting value) {
    setState(() {
      _configuration = value;
    });
  }

  ThemeData get theme {
    switch(_configuration.theme) {
      case ThemeColor.standard:
        return new ThemeData(
          primarySwatch: Colors.blueGrey,
        );
      case ThemeColor.grey:
        return new ThemeData(
          primarySwatch: Colors.grey,
        );
      case ThemeColor.green:
        return new ThemeData(
          primarySwatch: Colors.green,
        );
      case ThemeColor.red:
        return new ThemeData(
          primarySwatch: Colors.red,
        );
      case ThemeColor.blue:
        return new ThemeData(
          primarySwatch: Colors.blue,
        );
      case ThemeColor.purple:
        return new ThemeData(
          primarySwatch: Colors.purple,
        );
    }
    return new ThemeData(
      primarySwatch: Colors.blueGrey,
    );
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        title: 'TV Lige Nu!',
        theme: theme,
        home: new ChannelList(title: 'TV Lige Nu!'),
        routes: <String, WidgetBuilder>{
          '/settings': (BuildContext context) => new Settings(_configuration, configurationUpdater)
        },
    );
  }
}
