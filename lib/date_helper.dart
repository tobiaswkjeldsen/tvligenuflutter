import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';

class DateHelper {
  static DateFormat _formatHourMinutes = new DateFormat('HH:mm');
  static DateFormat _formatDate = new DateFormat('EEEE');

  static initLocale() {
    Intl.defaultLocale = 'da_DK';
    initializeDateFormatting('da_DK');
  }

  static String hoursMinutes(DateTime dateTime) {
    return _formatHourMinutes.format(dateTime.toLocal());
  }

  static String todayTomorrowOrWeekDay(DateTime dateTime) {
    dateTime = dateTime.toLocal();
    if(dateTime.day == new DateTime.now().day) {
      return "i dag";
    } else if(dateTime.day == new DateTime.now().day+1) {
      return "i morgen";
    } else {
      return "${_formatDate.format(dateTime)}";
    }
  }

  static String todayTomorrowOrDateWithMinutes(DateTime dateTime) {
    return "${todayTomorrowOrWeekDay(dateTime)} ${hoursMinutes(dateTime)}";
  }

}
