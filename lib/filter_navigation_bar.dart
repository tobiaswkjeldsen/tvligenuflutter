import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tvligenuflutter/filter_list.dart';

class FilterNavigationBar extends StatelessWidget {

  FilterNavigationBar({this.channelName:""});

  final channelName;

  _onFilterPressed(BuildContext context, String title, String category) {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        return new FilterList(
          title: title,
          category: category,
          channelName: channelName,
        );
      },
    ));
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      decoration: new BoxDecoration(
        boxShadow: [new BoxShadow(blurRadius: 8.0, color: Colors.black12)],
        color: Colors.white,
      ),
      padding: new EdgeInsets.symmetric(vertical: 10.0),
      child: new Row(
        children: <Widget>[
          new InkWell(
            child: new Chip(
                label: new Text(
                  "Film",
                )),
            onTap: () {
              _onFilterPressed(context, "Film", "movie");
            },
          ),
          new InkWell(
            child: new Chip(label: new Text("Serier")),
            onTap: () {
              _onFilterPressed(context, "Serier", "series");
            },
          ),
          new InkWell(
            child: new Chip(label: new Text("Sport")),
            onTap: () {
              _onFilterPressed(context, "Sport", "sport");
            },
          ),
          new InkWell(
            child: new Chip(label: new Text("Dokumentar")),
            onTap: () {
              _onFilterPressed(context, "Dokumentar", "documentary");
            },
          ),
        ],
        mainAxisAlignment: MainAxisAlignment.spaceAround,
      ),
    );
  }

}