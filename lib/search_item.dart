import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:tvligenuflutter/date_helper.dart';
import 'package:tvligenuflutter/tv_assembler.dart';
import 'package:tvligenuflutter/time.dart';

typedef void SearchItemActionCallback(Program program);

class SearchItem extends StatelessWidget {
  final Program program;
  final SearchItemActionCallback onPressed;

  SearchItem({Key key, this.program, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Container(
        padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 2.0),
    child: new Material(
        borderRadius: new BorderRadius.all(new Radius.circular(4.0)),
        elevation: 2.0,
        type: MaterialType.card,
        child:  new InkWell(
                onTap: () {
                  onPressed(program);
                },
                child: new Container(
                    decoration: new BoxDecoration(
                        border: new Border(
                            bottom: new BorderSide(
                                color: Theme.of(context).dividerColor))),
                    padding: const EdgeInsets.fromLTRB(20.0, 16.0, 20.0, 16.0),
                    child: new Column(
                        children: [
                      new Text(program.channelName,
                          style: new TextStyle(
                              color: Colors.black54,
                              fontSize: 19.0,
                              fontWeight: FontWeight.w400)),
                      new Padding(padding: const EdgeInsets.all(5.0)),
                      new Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                      new Time(time: program.airTime),
                        new Padding(padding: const EdgeInsets.all(5.0)),
                        new Expanded(child:                         new Text(program.programName,
                            style: new TextStyle(
                                color: Colors.black,
                                fontSize: 19.0,
                                fontWeight: FontWeight.w300))
                        )
                      ],
                      ),
                    new Padding(padding: const EdgeInsets.all(5.0)),
                      new Text(
                          "Programmet sendes ${DateHelper
                              .todayTomorrowOrWeekDay(
                              program.airTime)}",
                          style: new TextStyle(
                              color: Colors.grey,
                              fontSize: 15.0,
                              fontWeight: FontWeight.w300))
                    ], crossAxisAlignment: CrossAxisAlignment.start)))));
  }
}
