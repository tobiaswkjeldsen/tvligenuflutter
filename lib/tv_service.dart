import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:tvligenuflutter/tv_assembler.dart';

class TVService {
  var _httpClient = new HttpClient();
  var _tvAssembler = new TVAssembler();

  Future<String> _get(String url, [Map<String, String> queryParameters]) async {
    var uri = new Uri.http('wkjeldsen.dk', url, queryParameters);
    var request = await _httpClient.getUrl(uri);
    var response = await request.close();
    return response.transform(latin1.decoder).join();
  }

  Future<List<Channel>> getChannels() {
    return _get('tv/tv_v9.php', {'side': 'oversigt'}).then((response) {
      return _tvAssembler.channels(response);
    });
  }

  Future<List<Program>> searchPrograms(
      {String searchTerm: "",
      String category: "",
      int day: 0,
      String channelName: ""}) {
    return _get('tv/tv_v9.php', {
      'filter': '1',
      'searchTerm': searchTerm,
      'category': category,
      'channel': channelName,
      'day': day.toString()
    }).then((response) {
      return _tvAssembler.searchResults(response);
    });
  }

  Future<List<Overview>> getOverview() {
    return _get('tv/tv_v9.php', {'overview': '1'}).then((response) {
      return _tvAssembler.overviews(response);
    });
  }

  Future<List<Program>> getPrograms(String channel, [int day = 0]) {
    return _get('tv/tv_v9.php', {
      'kanal': channel,
      'dag': day.toString()
    }).then((response) {
      List<Program> programs = _tvAssembler.programs(response, day);
      programs.forEach((program) => program.channelName = channel);
      return programs;
    });
  }

  Future<Description> getDescription(String programId) {
    return _get('tv/tv_v9.php', {'desc': programId}).then((response) {
      return _tvAssembler.description(response);
    });
  }
}
