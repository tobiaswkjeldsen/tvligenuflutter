import 'dart:async';
import 'package:flutter/material.dart';
import 'package:tvligenuflutter/settings_service.dart';
import 'package:tvligenuflutter/tv_assembler.dart';
import 'package:tvligenuflutter/tv_service.dart';

class _FavoriteChannelsState extends State<FavoriteChannels> {
  var _tvService = new TVService();
  var _settingsService = new SettingsService();

  List<Channel> _channels = new List<Channel>();
  List<Channel> _channelsOrig = new List<Channel>();
  List<Channel> _channelsNew;

  bool _isSearching = false;

  @override
  void initState() {
    super.initState();

    _loadNewChannels();
  }

  void _refreshState() {
    setState(() {
      _channels = _channels;
      _isSearching = _isSearching;
    });
  }

  void _moveChannel(Channel channel, bool up) {
    var index = _channelsOrig.indexOf(channel);
    var moveIndex = up ? index - 1 : index + 1;
    var prevChannel = _channelsOrig[moveIndex];
    _channelsOrig[index] = prevChannel;
    _channelsOrig[moveIndex] = channel;

    _saveChannels();

    _refreshState();
  }

  void _enableChannel(enable, Channel channel) {
    channel.enabled = enable;

    _saveChannels();
  }

  Future _loadNewChannels() async {
    _channelsNew = await _tvService.getChannels();

    _mergeChannels();
  }

  Future _saveChannels() async {
    await _settingsService.saveChannels(_channelsOrig
        .where((channel) => channel.enabled)
        .toList(growable: false));

    _mergeChannels();

    if (_isSearching) {
      Navigator.pop(context);
    }
  }

  Future _mergeChannels() async {
    _channels.clear();

    _channels.addAll(await _settingsService.getChannels());

    _channelsNew.forEach((channelNew) {
      bool foundChannelNew = false;
      _channels.forEach((channel) {
        if (channel.name == channelNew.name) {
          foundChannelNew = true;
        }
      });
      if (!foundChannelNew) {
        _channels.add(new Channel(name: channelNew.name));
      }
    });

    _channelsOrig.clear();
    _channelsOrig.addAll(_channels);

    _refreshState();
  }

  void _searchChannels(String searchTerm) {
    _channels.clear();

    final RegExp regexp = new RegExp(searchTerm, caseSensitive: false);
    if (searchTerm.isNotEmpty) {
      _channels.addAll(_channelsOrig
          .where((Channel channel) => channel.name.contains(regexp)));
    } else {
      _channels.addAll(_channelsOrig);
    }

    _refreshState();
  }

  void _handleSearchChannelsBegin() {
    ModalRoute.of(context).addLocalHistoryEntry(new LocalHistoryEntry(
      onRemove: () {
        _isSearching = false;
      },
    ));
    _isSearching = true;

    _refreshState();
  }

  AppBar get _buildAppBar => new AppBar(
        title: new Text("Favoritter"),
        actions: [
          new IconButton(
            icon: const Icon(Icons.search),
            onPressed: _handleSearchChannelsBegin,
          ),
        ],
      );

  AppBar get _buildSearchBar => new AppBar(
        leading: new IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            _saveChannels();
          },
        ),
        title: new TextField(
            style: new TextStyle(fontSize: 17.0, color: Colors.white),
            autofocus: true,
            decoration: const InputDecoration(
                hintText: 'Søg kanaler',
                hintStyle:
                    const TextStyle(fontSize: 17.0, color: Colors.white)),
            onChanged: (searchTerm) => _searchChannels(searchTerm)),
      );

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: _isSearching ? _buildSearchBar : _buildAppBar,
        body: new ListView(
          padding: new EdgeInsets.symmetric(vertical: 8.0),
          children: _channels.map((Channel channel) {
            Widget trailing;
            if (channel.enabled && !_isSearching) {
              trailing =
                  new Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                new IconButton(
                    icon: new Icon(Icons.arrow_drop_up),
                    onPressed: () => _moveChannel(channel, true)),
                new IconButton(
                    icon: new Icon(Icons.arrow_drop_down),
                    onPressed: () => _moveChannel(channel, false)),
                new Checkbox(
                    value: channel.enabled,
                    onChanged: (value) => _enableChannel(value, channel)),
              ]);
            } else {
              trailing = new Column(children: [
                new Checkbox(
                    value: channel.enabled,
                    onChanged: (value) => _enableChannel(value, channel)),
              ]);
            }
            return new Column(children: <Widget>[
              new ListTile(
                  leading: new Text(
                    channel.name,
                    style: new TextStyle(
                      color: Colors.black54,
                      fontSize: 20.0,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  trailing: trailing),
              new Divider(
                color: Theme.of(context).dividerColor,
              ),
            ]);
          }).toList(),
        ));
  }
}

class FavoriteChannels extends StatefulWidget {
  FavoriteChannels({Key key}) : super(key: key);

  @override
  _FavoriteChannelsState createState() => new _FavoriteChannelsState();
}
