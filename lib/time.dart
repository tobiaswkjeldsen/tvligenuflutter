import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:tvligenuflutter/date_helper.dart';
import 'package:tvligenuflutter/tv_assembler.dart';

typedef void SearchItemActionCallback(Program program);

class Time extends StatelessWidget {
  final DateTime time;
  final Color color;

  Time({Key key, this.time, this.color: Colors.black}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Row(
      children: <Widget>[
      new Text(
          DateHelper
              .hoursMinutes(time)
              .substring(0, 2),
          style: new TextStyle(
              fontSize: 19.0, fontWeight: FontWeight.w400, color: color)),
      new Text(
          DateHelper
              .hoursMinutes(time)
              .substring(2, 5),
          style: new TextStyle(
              fontSize: 16.0, fontWeight: FontWeight.w300, color: color)),
    ],);
  }
}
