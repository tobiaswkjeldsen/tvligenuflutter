import 'dart:async';
import 'package:flutter/material.dart';
import 'package:tvligenuflutter/alarm_list.dart';
import 'package:tvligenuflutter/channel_item_small.dart';
import 'package:tvligenuflutter/fav_channels.dart';
import 'package:tvligenuflutter/filter_navigation_bar.dart';
import 'package:tvligenuflutter/program_list.dart';
import 'package:tvligenuflutter/search_list.dart';
import 'package:tvligenuflutter/settings_service.dart';
import 'package:tvligenuflutter/tv_assembler.dart';
import 'package:tvligenuflutter/tv_service.dart';

class _ChannelListState extends State<ChannelList> {
  var _tvService = new TVService();
  var _settingsService = new SettingsService();

  List<Overview> _overviews = new List<Overview>();

  _ChannelListState() {
    _fetchChannels();
  }

  Future<Null> _fetchChannels() async {

    List<Overview> overview = await _tvService.getOverview();

    List<Channel> channels = await _settingsService.getChannels();

    _overviews.clear();

    // Filter channels according to selected channels from settings
    channels.forEach((channel) {
      overview.forEach((overview) {
        if (channel.name == overview.channelName) {
          _overviews.add(overview);
        }
      });
    });

    // If no channels added add all
    if (channels.length == 0) {
      _overviews.addAll(overview);
    }

    setState(() {
      _overviews = _overviews;
    });
  }

  void _onAlarmsPressed() {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        return new AlarmList();
      },
    ));
  }

  void _onSearchPressed() {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        return new SearchList();
      },
    ));
  }

  void _onFavoritesPressed() {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        return new FavoriteChannels();
      },
    ));
  }

  void _onSettingsPressed() {
    Navigator.pushNamed(context, '/settings');
  }

  _onChannelPressed(BuildContext context, Overview overview) {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        return new ProgramList(overview: overview);
      },
    ));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text(widget.title), actions: [
        new IconButton(
            icon: new Icon(Icons.alarm, color: Colors.white),
            onPressed: _onAlarmsPressed),
        new IconButton(
            icon: new Icon(Icons.star, color: Colors.white),
            onPressed: _onFavoritesPressed),
        new IconButton(
            icon: new Icon(Icons.settings, color: Colors.white),
            onPressed: _onSettingsPressed),
      ]),
      body: new RefreshIndicator(
          child: new ListView(
            children: _overviews.map((Overview overview) {
              return new ChannelItemSmall(
                overview: overview,
                onPressed: (Overview overview) {
                  _onChannelPressed(context, overview);
                },
              );
            }).toList(),
          ),
          onRefresh: _fetchChannels),
      floatingActionButton: new FloatingActionButton(
        onPressed: _onSearchPressed,
        child: new Icon(Icons.search),
      ),
      bottomNavigationBar: new FilterNavigationBar(),
    );
  }
}

class ChannelList extends StatefulWidget {
  ChannelList({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ChannelListState createState() => new _ChannelListState();
}
