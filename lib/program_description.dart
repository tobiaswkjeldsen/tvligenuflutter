import 'dart:async';
import 'package:flutter/material.dart';
import 'package:tvligenuflutter/alarm_service.dart';
import 'package:tvligenuflutter/date_helper.dart';
import 'package:tvligenuflutter/tv_assembler.dart';
import 'package:tvligenuflutter/tv_service.dart';
import 'package:url_launcher/url_launcher.dart';

class _ProgramDescriptionState extends State<ProgramDescription> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  var _tvService = new TVService();
  var _alarmService = new AlarmService();

  Program _program;
  Description _programDescription = new Description();
  bool _hasAlarm = false;

  _ProgramDescriptionState(program) {
    this._program = program;
  }

  @override
  void initState() {
    super.initState();

    _fetchDescription();

    _fetchAlarm();
  }

  void _fetchDescription() {
    _tvService.getDescription(_program.id).then((description) {
      setState(() {
        _programDescription = description;
      });
    });
  }

  void _setAlarmState() {
    setState(() {
      _hasAlarm = _alarmService.isRegistered(_program);
    });
  }

  Future _fetchAlarm() async {
    await _alarmService.init();

    _setAlarmState();
  }

  Future _registerAlarm() async {
    await _alarmService.register(_program);

    _setAlarmState();
  }

  Future _unregisterAlarm() async {
    await _alarmService.unregister(_program);

    _setAlarmState();
  }

  Future _launchImdb() async {
    var url = 'imdb:///find?q=${_program.programName}';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        title: new Text(_program.channelName),
      ),
      body: new ListView(children: [
        new Container(
            child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
              new SizedBox(
                height: 190.0,
                child: new Stack(
                  fit: StackFit.expand,
                  children: <Widget>[
                    new Positioned.fill(
                      child: new FadeInImage.assetNetwork(
                        placeholder: 'images/placeholder.png',
                        image: _programDescription.graphicsUrl,
                        fit: BoxFit.cover,
                      ),
                    ),
                    const DecoratedBox(
                      decoration: const BoxDecoration(
                        gradient: const LinearGradient(
                          begin: Alignment.bottomCenter,
                          end: Alignment.topCenter,
                          colors: const <Color>[
                            Colors.black87,
                            Colors.transparent
                          ],
                        ),
                      ),
                    ),
                    new Positioned(
                      bottom: 5.0,
                      left: 20.0,
                      right: 20.0,
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(
                            _program.programName,
                            style: new TextStyle(
                                color: Colors.white,
                                fontSize: 20.0,
                                fontWeight: FontWeight.w400),
                          ),
                          new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                new Text(
                                    "Programmet starter ${DateHelper
                                        .todayTomorrowOrDateWithMinutes(
                                        _program.airTime)}",
                                    style: new TextStyle(
                                        color: Colors.grey,
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.w300)),
                                new IconButton(
                                    color: Colors.white,
                                    icon: new ImageIcon(
                                        new AssetImage("images/imdb.png")),
                                    onPressed: () {
                                      _launchImdb();
                                    }),
                                new IconButton(
                                  color: Colors.white,
                                  icon: _hasAlarm
                                      ? new Icon(Icons.alarm_off)
                                      : new Icon(Icons.alarm_add),
                                  onPressed: () {
                                    _hasAlarm ? _unregisterAlarm() : _registerAlarm();
                                    scaffoldKey.currentState.showSnackBar(new SnackBar(duration: new Duration(seconds: 5),content: new Text('"${_program.programName}" ${_hasAlarm ? 'fjernet som alarm' : '${DateHelper
                                        .todayTomorrowOrDateWithMinutes(
                                        _program.airTime)} tilføjet som alarm'}')));
                                  },
                                )
                              ])
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              new Container(
                padding: const EdgeInsets.fromLTRB(20.0, 16.0, 20.0, 16.0),
                child: new Column(
                  children: <Widget>[
                    new Text(_programDescription.description,
                        style: new TextStyle(
                            color: Colors.black,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w300)),
                  ],
                ),
              ),
            ])),
      ]),
    );
  }
}

class ProgramDescription extends StatefulWidget {
  ProgramDescription({Key key, this.program}) : super(key: key);

  final Program program;

  @override
  _ProgramDescriptionState createState() =>
      new _ProgramDescriptionState(program);
}
