import 'dart:async';

import 'package:flutter/material.dart';
import 'package:tvligenuflutter/program_description.dart';
import 'package:tvligenuflutter/search_item.dart';
import 'package:tvligenuflutter/settings_service.dart';
import 'package:tvligenuflutter/tv_assembler.dart';
import 'package:tvligenuflutter/tv_service.dart';

class _SearchListState extends State<SearchList> {
  var _tvService = new TVService();
  var _settingsService = new SettingsService();

  final TextEditingController _controller = new TextEditingController();
  List<Program> _programs = new List<Program>();

  Future _searchPrograms(String searchTerm) async {
    _programs.clear();

    if (searchTerm.length > 3) {

      List<Program> programs = await _tvService.searchPrograms(searchTerm: searchTerm);

      List<Channel> channels = await _settingsService.getChannels();

      channels.forEach((channel) {
        programs.forEach((program) {
          if (channel.name == program.channelName) {
            _programs.add(program);
          }
        });
      });

      // If not channels add all
      if (channels.length == 0) {
        _programs.addAll(programs);
      }

    }

    setState((){
      _programs = _programs;
    });
  }

  void _onProgramPressed(BuildContext context, Program program) {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        return new ProgramDescription(program: program);
      },
    ));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new TextField(
              style: new TextStyle(fontSize: 17.0, color: Colors.white),
              autofocus: true,
              controller: _controller,
              decoration: new InputDecoration(
                  hintText: 'Søg efter program',
                  hintStyle:
                      new TextStyle(fontSize: 17.0, color: Colors.white)),
              onChanged: (searchTerm) => _searchPrograms(searchTerm)),
        ),
        body: new ListView(
          children: _programs.map((Program program) {
            return new SearchItem(
              program: program,
              onPressed: (Program program) {
                _onProgramPressed(context, program);
              },
            );
          }).toList(),
        ));
  }
}

class SearchList extends StatefulWidget {
  SearchList({Key key}) : super(key: key);

  @override
  _SearchListState createState() => new _SearchListState();
}
