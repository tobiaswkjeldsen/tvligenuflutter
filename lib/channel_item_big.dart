import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:tvligenuflutter/date_helper.dart';
import 'package:tvligenuflutter/tv_assembler.dart';

typedef void ChannelItemActionCallback(Overview overview);

class ChannelItemBig extends StatelessWidget {
  ChannelItemBig({Key key, this.overview, this.onPressed}) : super(key: key);

  final Overview overview;
  final ChannelItemActionCallback onPressed;

  @override
  Widget build(BuildContext context) {
    var length = overview.airTimeNext.millisecondsSinceEpoch -
        overview.airTime.millisecondsSinceEpoch;
    var progress = overview.airTimeNext.millisecondsSinceEpoch -
        new DateTime.now().millisecondsSinceEpoch;
    double percent = (((length - progress) * 100) / length) / 100;
    return new Container(
        padding: const EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 2.0),
        child: new Hero(tag: overview, child: new Card(
            child: new InkWell(
                onTap: () {
                  onPressed(overview);
                },
                child: new Container(
                    padding: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 20.0),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        new Text(overview.channelName,
                            style: new TextStyle(
                              color: Colors.black54,
                              fontSize: 20.0,
                              fontWeight: FontWeight.w400,
                            )),
                        new Padding(padding: const EdgeInsets.all(5.0)),
                        new Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            new Text(DateHelper.hoursMinutes(overview.airTime),
                                style: new TextStyle(
                                    color: Colors.grey,
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.w300)),
                            new Padding(padding: const EdgeInsets.all(5.0)),
                            new Text(overview.programNameNow,
                                style: new TextStyle(
                                    color: Colors.black54,
                                    fontSize: 19.0,
                                    fontWeight: FontWeight.w300)),
                          ],
                        ),
                        new Padding(padding: const EdgeInsets.all(2.5)),
                        new LinearProgressIndicator(value: percent),
                        new Padding(padding: const EdgeInsets.all(5.0)),
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            new Text(DateHelper.hoursMinutes(overview.airTimeNext),
                                style: new TextStyle(
                                    color: Colors.grey,
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.w300
                                )),
                            new Padding(padding: const EdgeInsets.all(5.0)),
                            new Text(overview.programNameNext,
                                style: new TextStyle(
                                    color: Colors.black54,
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.w300)),
                          ],
                        ),
                      ],
                    ))))));
  }
}
