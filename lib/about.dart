import 'dart:async';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class _AboutState extends State<About> {

  Future _launchBitbucket() async {
    var url = 'http://wkjeldsen.dk/tvligenu';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text("Om")),
      body: new Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const ListTile(
              title: const Text('TV Lige Nu!'),
              subtitle: const Text('Programoversigt over udvalgte danske TV-kanaler.'),
            ),
            const ListTile(
              title: const Text('Udvikler'),
              subtitle: const Text('Tobias Westergaard Kjeldsen <tobias@wkjeldsen.dk>'),
            ),
            new ListTile(
              title: const Text('Mere info'),
              subtitle: const Text('http://wkjeldsen.dk/tvligenu'),
              onTap: () {_launchBitbucket();},
            ),
          ],
        ),
    );
  }
}

class About extends StatefulWidget {
  About({Key key}) : super(key: key);

  @override
  _AboutState createState() => new _AboutState();
}
