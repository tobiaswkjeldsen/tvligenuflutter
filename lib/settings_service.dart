import 'dart:async';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvligenuflutter/tv_assembler.dart';

class SettingsService {
  final PREF_PROGRAM_ALARMS = 'programAlarms';
  final PREF_CHANNELS = 'channels';
  final PREF_THEME = 'theme';
  final PREF_ALARM_OFFSET = 'alarmOffset';

  Future<SharedPreferences> getPreferences() async {
    return SharedPreferences.getInstance();
  }

  Future<Null> saveAlarmOffset(int offset) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setInt(PREF_ALARM_OFFSET, offset);
  }

  Future<int> getAlarmOffsetMinutes() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getInt(PREF_ALARM_OFFSET) ?? 0;
  }

  Future<int> getTheme() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getInt(PREF_THEME) ?? 0;
  }

  Future<Null> saveTheme(int theme) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setInt(PREF_THEME, theme);
  }

  Future<Null> saveChannels(List<Channel> channels) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(PREF_CHANNELS,
        json.encode(new ChannelList(channels: channels).toJson()));
  }

  Future<List<Channel>> getChannels() async {
    SharedPreferences sharedPreferences = await getPreferences();
    String data = sharedPreferences.getString(PREF_CHANNELS);
    return data != null
        ? ChannelList.fromJson(json.decode(data)).channels
        : new List<Channel>();
  }

  Future<Null> saveProgramAlarms(List<ProgramAlarm> programAlarms) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(
        PREF_PROGRAM_ALARMS,
        json.encode(
            new ProgramAlarmList(programAlarms: programAlarms).toJson()));
  }

  Future<List<ProgramAlarm>> getProgramAlarms() async {
    SharedPreferences sharedPreferences = await getPreferences();
    String data = sharedPreferences.getString(PREF_PROGRAM_ALARMS);
    return data != null
        ? ProgramAlarmList.fromJson(json.decode(data)).programAlarms
        : new List<ProgramAlarm>();
  }
}
