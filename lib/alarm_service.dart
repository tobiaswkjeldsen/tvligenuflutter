import 'dart:async';
import 'package:tvligenuflutter/settings_service.dart';
import 'package:tvligenuflutter/tv_assembler.dart';
import 'package:scheduled_notifications/scheduled_notifications.dart';

class AlarmService {
  var _settingsService = new SettingsService();

  List<ProgramAlarm> _programAlarms;

  Future init() async {
    _programAlarms = await _settingsService.getProgramAlarms();

    _unregisterOld();
  }

  Future register(Program program) async {
    int alarmOffsetMinutes = await _settingsService.getAlarmOffsetMinutes();
    int notificationId = await ScheduledNotifications.scheduleNotification(
        program.airTime.subtract(new Duration(minutes: alarmOffsetMinutes)).millisecondsSinceEpoch,
        "${program.programName} starter kl. ${program.airTimeText} på ${program.channelName}",
        program.programName,
        "Starter kl. ${program.airTimeText} på ${program.channelName}");

    _programAlarms.add(
        new ProgramAlarm(program: program, notificationId: notificationId));

    _settingsService.saveProgramAlarms(_programAlarms);
  }

  Future unregister(Program program) async {
    ProgramAlarm programAlarm = _programAlarms
        .singleWhere((programAlarm) => programAlarm.program.id == program.id);

    await ScheduledNotifications
        .unscheduleNotification(programAlarm.notificationId);

    _programAlarms.remove(programAlarm);

    _settingsService.saveProgramAlarms(_programAlarms);
  }

  _unregisterOld() async {
    int alarmOffsetMinutes = await _settingsService.getAlarmOffsetMinutes();
    _programAlarms.removeWhere((programAlarm) =>
        programAlarm.program.airTime.isBefore(new DateTime.now().add(new Duration(minutes: alarmOffsetMinutes))));

    _settingsService.saveProgramAlarms(_programAlarms);
  }

  bool isRegistered(Program program) {
    bool isRegistered = false;
    _programAlarms.forEach((programAlarm) {
      if (programAlarm.program.id == program.id) {
        isRegistered = true;
      }
    });
    return isRegistered;
  }

  List<ProgramAlarm> getProgramAlarms() {
    _programAlarms.sort((a,b) => a.program.airTime.compareTo(b.program.airTime));
    return _programAlarms;
  }

}
