import 'package:flutter/material.dart';
import 'package:tvligenuflutter/alarm_service.dart';
import 'package:tvligenuflutter/date_helper.dart';
import 'package:tvligenuflutter/filter_navigation_bar.dart';
import 'package:tvligenuflutter/program_description.dart';
import 'package:tvligenuflutter/program_item.dart';
import 'package:tvligenuflutter/search_list.dart';
import 'package:tvligenuflutter/tv_assembler.dart';
import 'package:tvligenuflutter/tv_service.dart';

class _ProgramListState extends State<ProgramList> {
  var _tvService = new TVService();

  DateTime _startDate = new DateTime(new DateTime.now().year,
      new DateTime.now().month, new DateTime.now().day);
  DateTime _currentDate = new DateTime(new DateTime.now().year,
      new DateTime.now().month, new DateTime.now().day);

  Overview _overview;
  List<Program> _programs = new List<Program>();

  _ProgramListState(Overview overview) {
    this._overview = overview;
  }

  @override
  void initState() {
    super.initState();

    _fetchPrograms();
  }

  void _pickDate() {
    showDatePicker(
        context: context,
        initialDate: _currentDate,
        firstDate: _startDate,
        lastDate: new DateTime.now().add(new Duration(days: 5))).then((date) {
      _currentDate = new DateTime(date.year, date.month, date.day);

      _fetchPrograms();
    });
  }

  void _previousDay() {
    _currentDate = _currentDate.subtract(new Duration(days: 1));

    _fetchPrograms();
  }

  void _nextDay() {
    _currentDate = _currentDate.add(new Duration(days: 1));

    _fetchPrograms();
  }

  int _dayIndex() {
    return _currentDate.difference(_startDate).inDays;
  }

  void _fetchPrograms() {
    _programs.clear();

    _tvService.getPrograms(_overview.channelName, _dayIndex()).then((programs) {
      setState(() {
        _programs.addAll(programs);
      });
    });
  }

  Row _getTitle() {
    return new Row(children: [
      new Flexible(child: new Text(widget.overview.channelName)),
      new Padding(padding: const EdgeInsets.all(5.0)),
      new Text(DateHelper.todayTomorrowOrWeekDay(_currentDate),
          style: new TextStyle(fontSize: 14.0))
    ]);
  }

  void _onSearchPressed() {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        return new SearchList();
      },
    ));
  }

  void _onProgramPressed(BuildContext context, program) {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        return new ProgramDescription(program: program);
      },
    ));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(title: _getTitle(), actions: [
          new IconButton(
              icon: new Icon(Icons.calendar_today, color: Colors.white),
              onPressed: _pickDate),
          new IconButton(
              icon: new Icon(Icons.chevron_left, color: Colors.white),
              onPressed: _previousDay),
          new IconButton(
              icon: new Icon(Icons.chevron_right, color: Colors.white),
              onPressed: _nextDay),
        ]),
        body: new ListView(
          children: _programs.map((Program program) {
            return new ProgramItem(
              overview: _programs.indexOf(program) == 0 && _dayIndex() == 0
                  ? _overview
                  : null,
              program: program,
              onPressed: (Program program) {
                _onProgramPressed(context, program);
              },
            );
          }).toList(),
        ),
        floatingActionButton: new FloatingActionButton(
          onPressed: _onSearchPressed,
          child: new Icon(Icons.search),
        ),
        bottomNavigationBar: new FilterNavigationBar(
          channelName: _overview.channelName,
        ));
  }
}

class ProgramList extends StatefulWidget {
  ProgramList({Key key, this.overview}) : super(key: key);

  final Overview overview;

  @override
  _ProgramListState createState() => new _ProgramListState(overview);
}
