import 'package:intl/intl.dart';
import 'package:json_annotation/json_annotation.dart';
part 'tv_assembler.g.dart';

@JsonSerializable()
class ProgramAlarmList extends Object with _$ProgramAlarmListSerializerMixin {
  List<ProgramAlarm> programAlarms;

  ProgramAlarmList({this.programAlarms});

  factory ProgramAlarmList.fromJson(Map<String, dynamic> json) => _$ProgramAlarmListFromJson(json);
}

@JsonSerializable()
class ProgramAlarm extends Object with _$ProgramAlarmSerializerMixin {
  int notificationId;
  Program program;

  ProgramAlarm({this.notificationId, this.program});

  factory ProgramAlarm.fromJson(Map<String, dynamic> json) => _$ProgramAlarmFromJson(json);
}

@JsonSerializable()
class ChannelList extends Object with _$ChannelListSerializerMixin {
  List<Channel> channels;

  ChannelList({this.channels});

  factory ChannelList.fromJson(Map<String, dynamic> json) => _$ChannelListFromJson(json);
}

@JsonSerializable()
class Channel extends Object with _$ChannelSerializerMixin {
  String name;
  bool enabled = false;

  Channel({this.name, this.enabled: false});

  factory Channel.fromJson(Map<String, dynamic> json) => _$ChannelFromJson(json);
}

@JsonSerializable()
class Program extends Object with _$ProgramSerializerMixin {
  int index;
  String channelName;
  String programName;
  String airTimeText;
  DateTime airTime;
  String airTimeEndText;
  DateTime airTimeEnd;
  String id;
  String graphicsUrl;

  Program(
      {this.index,
      this.channelName,
      this.programName,
      this.airTimeText,
      this.airTime,
      this.airTimeEndText,
      this.airTimeEnd,
      this.id,
      this.graphicsUrl});

  factory Program.fromJson(Map<String, dynamic> json) => _$ProgramFromJson(json);
}

class Overview {
  final String channelName;
  final String programNameNow;
  final String airTimeText;
  final DateTime airTime;
  final String programNameNext;
  final String airTimeNextText;
  final DateTime airTimeNext;

  const Overview(
      {this.channelName,
      this.programNameNow,
      this.airTimeText,
      this.airTime,
      this.programNameNext,
      this.airTimeNextText,
      this.airTimeNext});
}

class Description {
  String description;
  String graphicsUrl;

  Description({this.description = "", this.graphicsUrl = ""});
}

class TVAssembler {
  var dateFormatter = new DateFormat('HH:mm');
  var dateFullFormatter = new DateFormat('dd/MM/y HH:mm');

  List<Program> searchResults(String data) {
    var searchResults = new List<Program>();
    var searchResultsArr = data.split('\n');
    for (int i = 0; i < searchResultsArr.length - 1; i = i + 3)
      searchResults.add(new Program(
        programName: searchResultsArr[i].substring(18),
          airTime: dateFullFormatter
                  .parse(searchResultsArr[i].substring(0, 16)),
        airTimeText: searchResultsArr[i].substring(11, 16),
        id: searchResultsArr[i + 1],
        channelName: searchResultsArr[i + 2],
      ));
    return searchResults;
  }

  List<Channel> channels(String data) {
    var channels = new List<Channel>();
    var channelsArr = data.split('\n');
    for(var i = 0; i < channelsArr.length - 1; i++) {
      channels.add(new Channel(name: channelsArr[i]));
    }
    return channels;
  }

  List<Overview> overviews(String data) {
    var overview = new List<Overview>();
    var overviewArr = data.split('\n');
    for (var i = 0; i < overviewArr.length - 1; i = i + 3) {
      overview.add(new Overview(
          channelName: overviewArr[i],
          programNameNow: overviewArr[i + 1].substring(18).trim(),
          airTimeText: overviewArr[i + 1].substring(11, 16),
          airTime: dateFullFormatter
                  .parse(overviewArr[i + 1].substring(0, 16)),
          programNameNext: overviewArr[i + 2].substring(18).trim(),
          airTimeNextText: overviewArr[i + 2].substring(11, 16),
          airTimeNext: dateFullFormatter
                  .parse(overviewArr[i + 2].substring(0, 16))));
    }
    return overview;
  }

  List<Program> programs(String data, int day) {
    var programs = new List<Program>();
    var programsArr = data.split('\n');
    for (var i = 0; i < programsArr.length - 1; i = i + 3) {
      programs.add(new Program(
          index: i,
          programName: programsArr[i].substring(18).trim(),
          airTimeText: programsArr[i].substring(11, 16),
          airTime: dateFullFormatter
                  .parse(programsArr[i].substring(0, 16)),
          id: programsArr[i + 1],
          graphicsUrl: programsArr[i + 2]));
    }

    // we set an end time for each program based on the next program
    for (var i = 0; i < programs.length - 1; i++) {
      var programCurr = programs[i];
      var programNext = programs[i + 1];
      programCurr.airTimeEnd = programNext.airTime;
      programCurr.airTimeEndText = programNext.airTimeText;
    }
    return programs;
  }

  Description description(String data) {
    var descriptionArr = data.split('\n');
    return new Description(description: descriptionArr[0], graphicsUrl: descriptionArr[1]);
  }
}
