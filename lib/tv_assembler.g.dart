// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tv_assembler.dart';

// **************************************************************************
// Generator: JsonSerializableGenerator
// **************************************************************************

ProgramAlarmList _$ProgramAlarmListFromJson(Map<String, dynamic> json) =>
    new ProgramAlarmList(
        programAlarms: (json['programAlarms'] as List)
            ?.map((e) => e == null
                ? null
                : new ProgramAlarm.fromJson(e as Map<String, dynamic>))
            ?.toList());

abstract class _$ProgramAlarmListSerializerMixin {
  List<ProgramAlarm> get programAlarms;
  Map<String, dynamic> toJson() =>
      <String, dynamic>{'programAlarms': programAlarms};
}

ProgramAlarm _$ProgramAlarmFromJson(Map<String, dynamic> json) =>
    new ProgramAlarm(
        notificationId: json['notificationId'] as int,
        program: json['program'] == null
            ? null
            : new Program.fromJson(json['program'] as Map<String, dynamic>));

abstract class _$ProgramAlarmSerializerMixin {
  int get notificationId;
  Program get program;
  Map<String, dynamic> toJson() =>
      <String, dynamic>{'notificationId': notificationId, 'program': program};
}

ChannelList _$ChannelListFromJson(Map<String, dynamic> json) => new ChannelList(
    channels: (json['channels'] as List)
        ?.map((e) =>
            e == null ? null : new Channel.fromJson(e as Map<String, dynamic>))
        ?.toList());

abstract class _$ChannelListSerializerMixin {
  List<Channel> get channels;
  Map<String, dynamic> toJson() => <String, dynamic>{'channels': channels};
}

Channel _$ChannelFromJson(Map<String, dynamic> json) =>
    new Channel(name: json['name'] as String, enabled: json['enabled'] as bool);

abstract class _$ChannelSerializerMixin {
  String get name;
  bool get enabled;
  Map<String, dynamic> toJson() =>
      <String, dynamic>{'name': name, 'enabled': enabled};
}

Program _$ProgramFromJson(Map<String, dynamic> json) => new Program(
    index: json['index'] as int,
    channelName: json['channelName'] as String,
    programName: json['programName'] as String,
    airTimeText: json['airTimeText'] as String,
    airTime: json['airTime'] == null
        ? null
        : DateTime.parse(json['airTime'] as String),
    airTimeEndText: json['airTimeEndText'] as String,
    airTimeEnd: json['airTimeEnd'] == null
        ? null
        : DateTime.parse(json['airTimeEnd'] as String),
    id: json['id'] as String,
    graphicsUrl: json['graphicsUrl'] as String);

abstract class _$ProgramSerializerMixin {
  int get index;
  String get channelName;
  String get programName;
  String get airTimeText;
  DateTime get airTime;
  String get airTimeEndText;
  DateTime get airTimeEnd;
  String get id;
  String get graphicsUrl;
  Map<String, dynamic> toJson() => <String, dynamic>{
        'index': index,
        'channelName': channelName,
        'programName': programName,
        'airTimeText': airTimeText,
        'airTime': airTime?.toIso8601String(),
        'airTimeEndText': airTimeEndText,
        'airTimeEnd': airTimeEnd?.toIso8601String(),
        'id': id,
        'graphicsUrl': graphicsUrl
      };
}
